from sys import stdin
from datetime import datetime

start = datetime.now()

accents = ['é', 'è', 'ê', 'ë', 'à', 'â', 'î', 'ï', 'ô', 'ù', 'û', 'ü', 'ÿ', 'æ', 'œ', 'ç']
replacements = ['e', 'e', 'e', 'e', 'a', 'a', 'i', 'i', 'o', 'u', 'u', 'u', 'y', 'ae', 'oe', 'c']

def convert_accent(w):
    w = list(w)
    for i, c in enumerate(w):
        if c in accents:
            w[i] = replacements[accents.index(c)]
    return ''.join(w)

assert convert_accent('âbç') == 'abc'

d = {}

line = stdin.readline().rstrip('\n').rstrip('\r')
while line != '':
    replaced = convert_accent(line)
    # print(f'Read {line}')
    signature = ''.join(sorted(replaced))
    if signature not in d:
        d[signature] = []
    d[signature].append(line)
    # print(d)
    line = stdin.readline().rstrip('\n').rstrip('\r')

na_max = 0
s_max = None
for s in d:
    na = len(d[s])
    if  na > na_max:
        s_max = s
        na_max = na

print("Mot avec le plus d'anagrammes :", d[s_max][0])
print(f'{na_max} anagrammes :', ' '.join(d[s_max]))

print(f'Script executed in {datetime.now() - start}')
