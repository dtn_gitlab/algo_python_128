t9 = '22233344455566677778889999'


def lettre_chiffre(x):
    """Convertir une lettre en un chiffre (str)
    """
    assert 'a' <= x <= 'z'
    return t9[ord(x) - ord('a')]


def mot_code(mot):
    """Convertir un mot en la séquence de chiffres qui le représente
    """
    return ''.join(map(lettre_chiffre, mot))


def predictive_text(dico):
    """Construire les propositions pour chaque séquence de chiffres
    """
    freq = {}
    for mot, poids in dico:
        prefix = ''
        for x in mot:
            prefix += x
            if prefix in freq:
                freq[prefix] += poids
            else:
                freq[prefix] = poids

    prop = {}
    for prefix in freq:
        code = mot_code(prefix)
        if code not in prop or freq[prop[code]] < freq[prefix]:
            prop[code] = prefix
    return prop


def propose(prop, seq):
    return prop.get(seq, 'None')


dico = [
    ('bonjour', 3),
    ('bombees', 10),
    ('bonduel', 9),
]

prop = predictive_text(dico)
print(prop)

print(propose(prop, '266'))
